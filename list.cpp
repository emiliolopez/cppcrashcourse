#include <iostream>
#include <cstdlib>

class List {
private:
	int _value;
	List* _next;

public:
	List(int value)
	{
		_value = value;
		_next = NULL;
	}

	~List();

	int Value()
	{
		return _value;
	}

	void Add(const List *l)
	{
		if (_next != NULL)
			_next->Add(l);
		else
			_next = (List*)l;
	}

	const List* GetLast()
	{
		// If there's some "next" element in this list
		// we need to go to the next link ...
		if (_next != NULL)
			return _next->GetLast();

		// ... otherwise
		return _next;
	}

	// Remove the last element, and return it
	List* Pop()
	{
		// I'm the last element, return myself
		if (_next == NULL)
			return this;

		//
		// There's a little muscle called 'brain' up there.
		// Use it for a change!
		// It's free, it's healthy and you can achieve wonders
		// with it!
		// ...

		// (TODO)
		return NULL;
	}

	size_t Length()
	{
		return 0; // TODO
	}
};

int main()
{
	List* head = new List(-1);

	// Add a handful of elements
	head->Add(new List(1));
	head->Add(new List(2));
	head->Add(new List(3));
	head->Add(new List(4));

	//
	// The code below should print this to stdout:
	// 4
	// 3
	// 2
	// 1
	// -1
	// -1
	//
	std::cout << head->Pop()->Value() << std::endl;
	std::cout << head->Pop()->Value() << std::endl;
	std::cout << head->Pop()->Value() << std::endl;
	std::cout << head->Pop()->Value() << std::endl;
	std::cout << head->Pop()->Value() << std::endl;
	std::cout << head->Pop()->Value() << std::endl;

	return EXIT_SUCCESS;
}

