#include <iostream>
#include <cstdlib>

class Operation {
// visible from Operation and all children, but not outside
protected:
	double _a;
	double _b;

// visible from everywhere
public:
	void Set(double x, double y) {

		_a = x;
		_b = y;
	}

	// "pure virtual" => must be implemented by inheriting classes
	virtual double Compute() = 0;
};

class Addition: public Operation {
public:
	double Compute() {
		return _a + _b;
	}
};

class Substraction: public Operation {
public:
	double Compute() {
		return _a - _b;
	}
};

// Now it's your turn!
// class Multiplication: public Operation
// class Division: public Operation

int main()
{
	Addition a;
	Substraction s;

	a.Set(1,2);
	s.Set(1,2);

	std::cout << "1 + 2 = " << a.Compute() << std::endl;
	std::cout << "1 - 2 = " << s.Compute() << std::endl;

	return EXIT_SUCCESS;
}
