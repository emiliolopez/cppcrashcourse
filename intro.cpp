#include <iostream>
#include <cstdlib>

class Calc {
public:
	/* C-style comments also work */
	double Add(double a, double b)
	{
		return a + b;
	}

	double Substract(double foo, double bar);
	//
	// ... please implement me!
	//
};


//
// main: this is your program's entry point
int main()
{
	// Instance of Class
	Calc c1;

	double res1 = c1.Add(1,2);

	// And another instance! Just for run because
	// the class is completely stateless
	Calc c2;

	double res2 = c2.Add(2,3);

	double res3 = c1.Add(1, c2.Add(2, c1.Add(3,4)));

	// OK, so now let's print the results
	std::cout << "1 + 2 is " << res1 << std::endl;
	std::cout << "2 + 3 is " << res2 << std::endl;
	std::cout << "1 + 2 + 3 + 4 is " << res3 << std::endl;

	// What's the output of: (2 * 3)?

	// See: http://www.cplusplus.com/reference/cstdlib/EXIT_SUCCESS/
	return EXIT_SUCCESS;
}

