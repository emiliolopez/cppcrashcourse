CC=g++
CFLAGS=-Wall -Wextra -pedantic
.PHONY: all clean

OBJS=helloworld.run customhowdy.run intro.run list.run inheritance.run

all: $(OBJS)
clean:
	rm -f $(OBJS)

%.run: %.cpp
	$(CC) -o $@ $< $(CFLAGS)
