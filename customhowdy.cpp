#include <iostream>
#include <string>

int main()
{
	/* variable definition */
	std::string name;

	/* Print something to the standard output. Notice the extra 
	   space as well as the lack of end line */
	std::cout << "What is your name? ";

	/* Read a string from standard input */
	std::cin >> name;

	/* You should know this by now :) */
	std::cout << "Howdy " << name << "!" << std::endl;

	return 0;
}
